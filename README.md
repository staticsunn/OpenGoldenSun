About
=====

This is OpenGoldenSun.

According to [The OpenGoldenSun FAQ](http://opengoldensun.com/faq/),

    This project uses C# and the MonoGame framework.

This project uses C and the SFML2 framework.

I found an empty [demo repository](https://github.com/opengoldensun/opengs-demo)
on Github, as well as [a C# code dump](https://github.com/immortaleeb/OpenGoldenSunWindows),
but I have not had time to look over it.

Requirements
============

 * SFML2 http://www.sfml-dev.org/

Setup
=====

    $ cd res
    $ sh get_spries.sh
    $ cd ..
    $ ./autogen.sh
    $ make

Future Plans
============

