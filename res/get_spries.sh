#!/bin/sh

SRC=http://www.goldensun-syndicate.net/sprites/
wget -c $SRC
grep -Eo '"[^"]+.zip"' index.html | tr -d '"' | \
    while read i
    do
        wget -c "${SRC}${i}"
    done

for f in *.zip
do
    unzip -o $f
done

find -name '*.gif' | while read i ; do convert $i "${i/gif/png}" ; optipng "${i/gif/png}" ; done
find -name '*.gif' -print0 | xargs -0 rm -fv
