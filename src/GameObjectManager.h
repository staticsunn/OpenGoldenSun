#pragma once

#include <algorithm>
#include <string>
#include <vector>

#include <ftw.h>
#include <fnmatch.h>

#include "VisibleGameObject.h"

static int loadAllMedia_callback (const char *fpath, const struct stat *sb, int typeflag);

static std::map <std::string, std::string> media_paths;

class GameObjectManager
{
public:
  GameObjectManager ();
  ~GameObjectManager ();

  VisibleGameObject *Add (std::string name, VisibleGameObject * gameObject);
  VisibleGameObject *Get (std::string name) const;
  void Remove (std::string name);
  void RemoveAll ();
  int GetObjectCount () const;

  void DrawAll (sf::RenderWindow & renderWindow);
  void UpdateAll ();

  void loadAllMedia ();
  void freeAllMedia ();

  struct GameObjectDeallocator
  {
    void operator  () (const std::pair <std::string, VisibleGameObject *> &p) const
    {
      delete p.second;
    }
  };

private:
  std::map <std::string, VisibleGameObject *> _gameObjects;
};
