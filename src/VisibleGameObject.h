#pragma once

#include <assert.h>

#include <iostream>

#include "Log.h"
#include "SFML.h"
#include "util.h"

class VisibleGameObject
{
public:
  VisibleGameObject ();
  VisibleGameObject (std::string);
  virtual ~VisibleGameObject ();

  virtual void Load (std::string filename);
  virtual void Draw (sf::RenderWindow & window);
  virtual void Update (float elapsedTime);

  virtual void SetPosition (float x, float y);
  virtual sf::Vector2f GetPosition () const;
  virtual bool IsLoaded () const;

  sf::Sprite & GetSprite ();
  sf::Image & GetImage ();

private:
  bool _isLoaded;
  sf::Image _image;
  sf::Sprite _sprite;
  sf::Texture _texture;
  std::string _filename;
};
