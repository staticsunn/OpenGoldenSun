#pragma once

#include <chrono>
#include <ctime>

#include "CGameEngine.h"
#include "CGameState.h"
#include "util.h"

class CMenuState:public CGameState
{
public:
  void Init (CGameEngine * game);
  void Cleanup (CGameEngine * game);

  void Pause ();
  void Resume ();

  void HandleEvents (CGameEngine * game);
  void Update (CGameEngine * game);
  void Draw (CGameEngine * game);

  static CMenuState *Instance ()
  {
    return &m_MenuState;
  }

protected:
  CMenuState ()
  {
  };

private:
  static CMenuState m_MenuState;

  std::chrono::high_resolution_clock::time_point timer;
};
