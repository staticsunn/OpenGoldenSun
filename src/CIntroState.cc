#include "CIntroState.h"
#include "CMenuState.h"

using namespace std::chrono;

CIntroState CIntroState::m_IntroState;

void
CIntroState::Init (CGameEngine * game)
{
  // nintendo = game->_gameObjectManager.Get ("res_backgrounds_gs2_Nintendo");
  nintendo = new VisibleGameObject ("res/backgrounds/gs2/Nintendo.png");
  obj = nintendo;

  for (ii = 0; ii < 4; ++ii)
  {
    std::string filename = "res/backgrounds/gs2/Camelot-" + to_string (ii) + ".png";
    // std::string filename = "res_backgrounds_gs2_Camelot-" + to_string (ii);
    // camelot[ii] = game->_gameObjectManager.Get (filename);
    camelot[ii] = new VisibleGameObject (filename);
    camelot[ii]->SetPosition (0, 0);
  }
  title_screen = new VisibleGameObject ("res/backgrounds/gs2/Title_Screen.png");
  // title_screen = game->_gameObjectManager.Get ("res_backgrounds_gs2_Title_Screen");
  title_screen->SetPosition (0, 0);

  end_time = high_resolution_clock::now() + std::chrono::seconds(3);

  debug ("CIntroState Init");
}

void
CIntroState::Cleanup (CGameEngine * game)
{
  debug ("CIntroState Cleanup");
}

void
CIntroState::Pause ()
{
  debug ("CIntroState Pause");
}

void
CIntroState::Resume ()
{
  debug ("CIntroState Resume");
}

void
CIntroState::HandleEvents (CGameEngine * game)
{
  sf::Event ev;

  if (game->window.pollEvent (ev))
  {
    switch (ev.type)
    {
    case sf::Event::Closed:
      exit (EXIT_SUCCESS);
      break;
    case sf::Event::EventType::KeyPressed:
      if (NINTENDO == state)
        state = CAMELOT;
      else if (CAMELOT == state)
        state = TITLE_SCREEN;
      else if (TITLE_SCREEN == state)
        game->ChangeState (CMenuState::Instance());
      break;
    default:
      break;
    }
  }
}

void
CIntroState::Update (CGameEngine * game)
{
  game->window.clear (sf::Color (0, 0, 0));

  switch (state)
  {
  case NINTENDO:
    {
      obj = nintendo;
      if (high_resolution_clock::now() > end_time)
      {
        // state = CAMELOT;
        end_time = high_resolution_clock::now() + std::chrono::seconds(3);
        end_time_c = high_resolution_clock::now() + std::chrono::milliseconds(100);
      }
    }
    break;
  case CAMELOT:
    {
      obj = camelot[ii];
      if (high_resolution_clock::now() > end_time_c)
      {
        end_time_c = high_resolution_clock::now() + std::chrono::milliseconds(100);
        if (++ii > 3)
          ii = 0;
      }
      if (high_resolution_clock::now() > end_time)
      {
        state = TITLE_SCREEN;
      }
    }
    break;
  case TITLE_SCREEN:
    {
      obj = title_screen;
    }
    break;
  }
}

void
CIntroState::Draw (CGameEngine * game)
{
  obj->Draw (game->window);
  game->window.display ();
}
