#include <signal.h>

#include <iostream>
#include <string>

#include <config.h>

#include "CGameEngine.h"

static void signal_callback_handler (int);

class OpenGS
{
private:
  CGameEngine game;
public:
  OpenGS ();
  ~OpenGS ();
};
