#pragma once

#include <vector>

#include "GameObjectManager.h"
#include "SFML.h"
#include "Log.h"

using namespace std;

class CGameState;

class CGameEngine
{
public:

  void Init (const char *title, int width = 240, int height = 160, int bpp =
             0, bool fullscreen = false);
  void Cleanup ();

  void ChangeState (CGameState * state);
  void PushState (CGameState * state);
  void PopState ();

  void HandleEvents ();
  void Update ();
  void Draw ();

  bool Running ()
  {
    return m_running;
  }
  void Quit ()
  {
    m_running = false;
  }

  void Display_InitGL ();
  int Display_SetViewport (int, int);
  void Display_Render ();

  sf::RenderWindow window;

  GameObjectManager _gameObjectManager;

  int m_width, m_height, m_bpp;

private:
  // the stack of states
  vector <CGameState *> states;

  bool m_running;
  bool m_fullscreen;
};
