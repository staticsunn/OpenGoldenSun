#include "OpenGS.h"
#include "CIntroState.h"

static void
signal_callback_handler (int signum)
{
  debug ("Caught signal %d", signum);

  // Cleanup and close up stuff here

  // Terminate program
  exit (signum);
}

OpenGS::OpenGS ()
{
  signal (SIGINT, signal_callback_handler);

  // initialize the engine
  game.Init (PACKAGE_STRING);

  // load the intro
  game.ChangeState (CIntroState::Instance ());

  // main loop
  while (game.Running ())
  {
    game.HandleEvents ();
    game.Update ();
    game.Draw ();
  }
}

OpenGS::~OpenGS ()
{
  // cleanup the engine
  game.Cleanup ();
}
