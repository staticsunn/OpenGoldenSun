#pragma once

#include "CGameEngine.h"

class CGameState
{
public:
  void Init ()
  {
  };
  virtual void Init (CGameEngine * game) = 0;
  virtual void Cleanup (CGameEngine * game) = 0;

  virtual void Pause () = 0;
  virtual void Resume () = 0;

  virtual void HandleEvents (CGameEngine * game) = 0;
  virtual void Update (CGameEngine * game) = 0;
  virtual void Draw (CGameEngine * game) = 0;

  void ChangeState (CGameEngine * game, CGameState * state)
  {
    game->ChangeState (state);
  }

protected:
  CGameState ()
  {
  }
};
