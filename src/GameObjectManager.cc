#include "GameObjectManager.h"

GameObjectManager::GameObjectManager ()
{
}

GameObjectManager::~GameObjectManager ()
{
  std::for_each (_gameObjects.begin (), _gameObjects.end (), GameObjectDeallocator ());
}

VisibleGameObject *
GameObjectManager::Add (std::string name, VisibleGameObject * gameObject)
{
  _gameObjects.insert (std::pair <std::string, VisibleGameObject *> (name, gameObject));
  return Get (name);
}

void
GameObjectManager::Remove (std::string name)
{
  std::map <std::string, VisibleGameObject *>::iterator results = _gameObjects.find (name);
  if (results != _gameObjects.end ())
  {
    delete results->second;
    _gameObjects.erase (results);
  }
}

void
GameObjectManager::RemoveAll ()
{
  for (std::map <std::string, VisibleGameObject *>::const_iterator it =
       _gameObjects.begin (); it != _gameObjects.end (); ++it)
  {
    delete it->second;
    _gameObjects.erase (it->first);
  }
}

VisibleGameObject *
GameObjectManager::Get (std::string name) const
{
  std::map <std::string, VisibleGameObject *>::const_iterator results = _gameObjects.find (name);
  if (results == _gameObjects.end ())
    return NULL;
  return results->second;

}

int
GameObjectManager::GetObjectCount () const
{
  return _gameObjects.size ();
}

void
GameObjectManager::DrawAll (sf::RenderWindow & renderWindow)
{
  std::map <std::string, VisibleGameObject *>::const_iterator itr = _gameObjects.begin ();
  while (itr != _gameObjects.end ())
  {
    itr->second->Draw (renderWindow);
    itr++;
  }
}

void
GameObjectManager::UpdateAll ()
{
  std::map <std::string, VisibleGameObject *>::const_iterator itr = _gameObjects.begin ();
  sf::Clock clock;
  float timeDelta;

  clock.getElapsedTime ();
  while (itr != _gameObjects.end ())
  {
    timeDelta = clock.restart ().asMicroseconds ();
    itr->second->Update (timeDelta);
    itr++;
  }
}

void
GameObjectManager::loadAllMedia ()
{
  std::string path = "res";
  if (ftw (path.c_str (), loadAllMedia_callback, 16))
  {
    path = ".." + os_path_sep + path;
    if (ftw (path.c_str (), loadAllMedia_callback, 16))
    {
      errx (1, ("Unable to open path: " + path).c_str ());
    }
  }
  for (std::map <std::string, std::string>::iterator it =
       media_paths.begin (); it != media_paths.end (); ++it)
  {
    debug ("Trying %s", it->second.c_str ());
    VisibleGameObject *obj = new VisibleGameObject (it->second);
    if (obj->IsLoaded())
    {
        Add (it->first, obj);
        debug ("Loaded %s (%s)", it->second.c_str (), it->first.c_str ());
    }
    else
        warn ("Failed to load %s", it->second.c_str ());
  }
}

void
GameObjectManager::freeAllMedia ()
{
  RemoveAll ();
}

int
loadAllMedia_callback (const char *fpath, const struct stat *sb, int typeflag)
{
  const char *filters[] = { "*.png" };
  /* if it's a file */
  if (typeflag == FTW_F)
  {
    int i;
    /* for each filter, */
    for (i = 0; i < sizeof (filters) / sizeof (filters[0]); i++)
    {
      /* if the filename matches the filter, */
      if (fnmatch (filters[i], fpath, FNM_CASEFOLD) == 0)
      {
        /* do something */
        debug ("found image: %s", fpath);
        std::string file_path = std::string (fpath);
        std::replace (file_path.begin (), file_path.end (), os_path_sep[0], '_'); // strip path separator
        while (file_path[0] == '.' || file_path[0] == os_path_sep[0] || file_path[0] == '_')
          file_path = file_path.substr (1, file_path.size ());  // strip leading shit
        file_path = file_path.substr (0, file_path.size () - 4);  // strip .png
        media_paths.insert (std::pair <std::string, std::string>(file_path, fpath));
        break;
      }
    }
  }

  /* tell ftw to continue */
  return 0;
}
