#pragma oncce

#include <chrono>
#include <ctime>

#include "CGameEngine.h"
#include "CGameState.h"

class CIntroState:public CGameState
{
public:
  void Init (CGameEngine * game);
  void Cleanup (CGameEngine * game);

  void Pause ();
  void Resume ();

  void HandleEvents (CGameEngine * game);
  void Update (CGameEngine * game);
  void Draw (CGameEngine * game);

  static CIntroState *Instance ()
  {
    return &m_IntroState;
  }

protected:
    CIntroState ()
  {
  }

private:
  static CIntroState m_IntroState;

  std::chrono::high_resolution_clock::time_point end_time, end_time_c;
  enum State {
      NINTENDO,
      CAMELOT,
      TITLE_SCREEN,
  };
  int ii;
  State state = NINTENDO;
  VisibleGameObject *nintendo;
  VisibleGameObject *camelot[4];
  VisibleGameObject *title_screen;
  VisibleGameObject *obj;
};
