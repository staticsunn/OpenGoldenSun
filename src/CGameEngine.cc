#include "CGameEngine.h"
#include "CGameState.h"

void
CGameEngine::Init (const char *title, int width, int height, int bpp,
                   bool fullscreen)
{
  int flags = 0;

  window.create (sf::VideoMode (width, height, bpp), title);

  Display_InitGL ();

  Display_SetViewport (width, height);

  _gameObjectManager.loadAllMedia();

  m_width = width;
  m_height = height;
  m_bpp = bpp;
  m_fullscreen = fullscreen;
  m_running = true;

  debug ("CGameEngine Init");
}

void
CGameEngine::Cleanup ()
{
  // cleanup the all states
  while (!states.empty ())
  {
    states.back ()->Cleanup (this);
    states.pop_back ();
  }

  // switch back to windowed mode so other 
  // programs won't get accidentally resized
  if (m_fullscreen)
  {
    debug ("I'm supposed to switch back to windowed mode here...");
  }

  _gameObjectManager.freeAllMedia();

  debug ("CGameEngine Cleanup");

  // shutdown
  window.close ();
}

void
CGameEngine::ChangeState (CGameState * state)
{
  // cleanup the current state
  if (!states.empty ())
  {
    states.back ()->Cleanup (this);
    states.pop_back ();
  }

  // store and init the new state
  states.push_back (state);
  states.back ()->Init (this);
}

void
CGameEngine::PushState (CGameState * state)
{
  // pause current state
  if (!states.empty ())
  {
    states.back ()->Pause ();
  }

  // store and init the new state
  states.push_back (state);
  states.back ()->Init ();
}

void
CGameEngine::PopState ()
{
  // cleanup the current state
  if (!states.empty ())
  {
    states.back ()->Cleanup (this);
    states.pop_back ();
  }

  // resume previous state
  if (!states.empty ())
  {
    states.back ()->Resume ();
  }
}


void
CGameEngine::HandleEvents ()
{
  // let the state handle events
  states.back ()->HandleEvents (this);
}

void
CGameEngine::Update ()
{
  // let the state update the game
  states.back ()->Update (this);
}

void
CGameEngine::Draw ()
{
  // let the state draw the screen
  states.back ()->Draw (this);
}

void
CGameEngine::Display_InitGL ()
{
  /* Enable smooth shading */
  glShadeModel (GL_SMOOTH);

  /* Set the background black */
  glClearColor (0.0f, 0.0f, 0.0f, 0.0f);

  /* Depth buffer setup */
  glClearDepth (1.0f);

  /* Enables Depth Testing */
  glEnable (GL_DEPTH_TEST);

  /* The Type Of Depth Test To Do */
  glDepthFunc (GL_LEQUAL);

  /* Really Nice Perspective Calculations */
  glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

/* function to reset our viewport after a window resize */
int
CGameEngine::Display_SetViewport (int width, int height)
{
  /* Height / width ration */
  GLfloat ratio;

  /* Protect against a divide by zero */
  if (height == 0)
  {
    height = 1;
  }

  ratio = (GLfloat) width / (GLfloat) height;

  /* Setup our viewport. */
  glViewport (0, 0, (GLsizei) width, (GLsizei) height);

  /* change to the projection matrix and set our viewing volume. */
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();

  /* Set our perspective */
  gluPerspective (45.0f, ratio, 0.1f, 100.0f);

  /* Make sure we're chaning the model view and not the projection */
  glMatrixMode (GL_MODELVIEW);

  /* Reset The View */
  glLoadIdentity ();

  return 1;
}
