#include "VisibleGameObject.h"

VisibleGameObject::VisibleGameObject ()
  : _isLoaded (false)
{
}

VisibleGameObject::VisibleGameObject (std::string filename)
{
  Load (filename);
  // assert (IsLoaded ());
}

VisibleGameObject::~VisibleGameObject ()
{
  _filename = "";
  _isLoaded = false;
}

void
VisibleGameObject::Load (std::string filename)
{
  if (_image.loadFromFile (filename) == false)
  {
    filename = ".." + os_path_sep + filename;
    if (_image.loadFromFile (filename) == false)
    {
      _filename = "";
      _isLoaded = false;
    }
    else
    {
      debug ("Loaded file from %s", filename.c_str());
      _texture.loadFromImage (_image);
      _sprite.setTexture (_texture);
      _filename = filename;
      _isLoaded = true;
    }
  }
}

void
VisibleGameObject::Draw (sf::RenderWindow & renderWindow)
{
  if (_isLoaded)
  {
    renderWindow.draw (_sprite);
  }
}

void
VisibleGameObject::Update (float elapsedTime)
{
}

void
VisibleGameObject::SetPosition (float x, float y)
{
  if (_isLoaded)
  {
    _sprite.setPosition (x, y);
  }
}

sf::Vector2f VisibleGameObject::GetPosition () const
{
  if (_isLoaded)
  {
    return _sprite.getPosition ();
  }
  return sf::Vector2f ();
}

sf::Sprite & VisibleGameObject::GetSprite ()
{
  return _sprite;
}

sf::Image & VisibleGameObject::GetImage ()
{
  return _image;
}

bool
VisibleGameObject::IsLoaded () const
{
  return _isLoaded;
}
