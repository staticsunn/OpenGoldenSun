#pragma once

#include <string>

#ifdef _WIN32
#define os_path_sep std::string("\\")
#else
#define os_path_sep std::string("/")
#endif
