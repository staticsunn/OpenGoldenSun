#include "Log.h"

void
log (FILE *fp, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  vfprintf(fp, _(fmt), ap);
  fputc('\n', fp);
  va_end (ap);
}

void
errx (int err, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  log (stderr, fmt, ap);
  va_end (ap);
  exit(err);
}
