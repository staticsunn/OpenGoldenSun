#pragma once

#include <stdio.h>
#include <stdarg.h>

#include "gettext.h"
#define _(string) gettext (string)

void log (FILE *fp, const char *fmt, ...);
void errx (int err, const char *fmt, ...);

static int _log_level = 0;

enum {
    LOG_NONE = 0,
    LOG_WARN = 1,
    LOG_DEBUG = 2,
};

#define warn(fmt, ...) do { \
    if (_log_level >= LOG_WARN) \
        log (stdout, fmt, ##__VA_ARGS__); \
} while (0)
#define debug(fmt, ...) do { \
    if (_log_level >= LOG_DEBUG) \
        log (stdout, fmt, ##__VA_ARGS__); \
} while (0)
