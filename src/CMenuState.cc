#include "CMenuState.h"

using namespace std::chrono;

CMenuState CMenuState::m_MenuState;

void
CMenuState::Init (CGameEngine * game)
{
  timer = high_resolution_clock::now () + std::chrono::milliseconds (10);

  debug ("CMenuState Init");
}

void
CMenuState::Cleanup (CGameEngine * game)
{
  debug ("CMenuState Cleanup");
}

void
CMenuState::Pause ()
{
  debug ("CMenuState Pause");
}

void
CMenuState::Resume ()
{
  debug ("CMenuState Resume");
}

void
CMenuState::HandleEvents (CGameEngine * game)
{
  sf::Event ev;

  if (game->window.pollEvent (ev))
  {
    switch (ev.type)
    {
    case sf::Event::Closed:
      exit (EXIT_SUCCESS);
      break;
    case sf::Event::EventType::KeyPressed:
      debug ("Key pressed!");
      break;
    default:
      break;
    }
  }
}

void
CMenuState::Update (CGameEngine * game)
{
  if (high_resolution_clock::now () > timer)
  {
    timer = high_resolution_clock::now () + std::chrono::milliseconds (10);
  }
  game->window.clear (sf::Color (0, 0, 0));
}

void
CMenuState::Draw (CGameEngine * game)
{
  game->window.display ();
}
